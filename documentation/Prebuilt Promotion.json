{
  "iapVersions": [
    "2023.2"
  ],
  "adapters": [
    {
      "isDependency": true,
      "name": "adapter-gitlab",
      "overview": "Adapter for Integration to GitLab",
      "webName": "Adapter for Integration to GitLab",
      "repoLink": "https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-gitlab",
      "version": "^0.13.5",
      "docLink": "https://docs.itential.com/opensource/docs/gitlab",
      "webLink": "https://www.itential.com/adapters/7846/"
    },
    {
      "isDependency": true,
      "name": "adapter-github",
      "overview": "Adapter for Integration to GitHub",
      "webName": "Adapter for Integration to GitHub",
      "repoLink": "https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-github",
      "version": "^0.6.5",
      "docLink": "https://docs.itential.com/opensource/docs/github-system",
      "webLink": "https://www.itential.com/adapters/github/"
    },
    {
      "isDependency": true,
      "name": "adapter-bitbucket",
      "overview": "Adapter for Integration to Atlassian Bitbucket Cloud",
      "webName": "Adapter for Integration to Atlassian Bitbucket Cloud",
      "repoLink": "https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-bitbucket",
      "version": "^0.5.7",
      "docLink": "https://docs.itential.com/opensource/docs/bitbucket",
      "webLink": "https://www.itential.com/adapters/bitbucket/"
    }
  ],
  "workflowProjects": [
    {
      "isDependency": false,
      "name": "Pre-Built Wizard",
      "webName": "Pre-Built Wizard",
      "version": "^2.0.7-2023.2.4",
      "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-wizard",
      "docLink": "",
      "webLink": "https://www.itential.com/automations/wizard/"
    }
  ],
  "externalDependencyList": [
    {
      "name": "App-Artifacts",
      "apiVersion": "^6.5.5-2023.2.2"
    },
    {
      "name": "GitLab"
    },
    {
      "name": "GitHub"
    },
    {
      "name": "Bitbucket"
    }
  ],
  "capabilities": [
    {
      "capability": "Create a new repository or update an existing repository by way of a merge request or pull request in the specified git version control platform i.e., GitLab, Github or Bitbucket Cloud for a Pre-Built as found on the local IAP where Prebuilt Promotion is being run."
    },
    {
      "capability": "Include the necessary configuration files and scripts to run a CI/CD pipeline if running Prebuilt Promotion against a repository that does not exist on the specified git version control service."
    }
  ],
  "entryPoint": {
    "name": "Prebuilt Promotion",
    "type": "Operations Manager Automation"
  },
  "inputs": [
    {
      "name": "versionControlService",
      "type": "string",
      "required": "yes",
      "description": "Version control adapter used to push the Pre-Built files to the respective version control service, i.e., Bitbucket Cloud, GitHub, or GitLab.",
      "exampleValue": "gitlab"
    },
    {
      "name": "projectName",
      "type": "string",
      "required": "yes",
      "description": "The project name of the repository as would be found in git version control service. Note that GitHub replaces the whitespace characters with \"-\" and unexpected behavior may occur if the GitHub project name has any whitespace characters. For Bitbucket Cloud, this name must be lowercase, alphanumerical, and may also contain underscores, dashes, or dots.",
      "exampleValue": "example-project"
    },
    {
      "name": "groupPath",
      "type": "string",
      "required": "yes",
      "description": "Link to the group or workspace to create the project under. Use Organization Name if this is a GitHub project. This is the name of the Workspace if using Bitbucket Cloud.",
      "exampleValue": "itentialopensource/pre-built-automations"
    },
    {
      "name": "prebuilt",
      "type": "string",
      "required": "yes",
      "description": "The Pre-Built from the IAP instance's Admin Essentials to push to the git version control service",
      "exampleValue": "@itentialopensource/example-prebuilt"
    },
    {
      "name": "reDiscoverPrebuilt",
      "type": "boolean",
      "required": "no",
      "description": "Whether or not to perform re-discovery of IAP components by reference from existing IAP components in the Pre-Built selected. This is needed if any IAP components have been added to or deleted from the Pre-Built since committing changes to version control service.",
      "exampleValue": "false"
    },
    {
      "name": "makeProjectPrivateGithubOnly",
      "type": "boolean",
      "required": "no",
      "description": "Whether or not the repository in GitHub is to be made private. This selection only applies if the repository does not already exist, and it is being created.",
      "exampleValue": "false"
    },
    {
      "name": "forExistingProjects",
      "type": "object",
      "required": "yes",
      "description": "Contain fields used for updating an existing project which includes \"mrType\", which is for the type of merge request change being made: patch, minor, or major. \"commitMessage\", which is commit message to use in version control commit. \"targetBranch\", the branch to commit the Pre-Built changes to.",
      "exampleValue": "{\n  \"mrType\": \"patch\",\n  \"commitMessage\": \"init commit\",\n  \"targetBranch\": \"master\"\n}"
    }
  ],
  "apiLinks": [
    {
      "title": "GitHub API",
      "link": "https://docs.github.com/en/rest?apiVersion=2022-11-28",
      "public": true
    },
    {
      "title": "GitLab API",
      "link": "https://docs.gitlab.com/ee/api/rest/",
      "public": true
    },
    {
      "title": "Atlassian - Bitbucket Cloud API",
      "link": "https://developer.atlassian.com/cloud/bitbucket/rest/intro",
      "public": true
    }
  ],
  "exampleInputsAndOutputs": [
    {
      "exampleInput": "{\n  \"formData\": {\n    \"checkInPrebuilt\": {\n      \"makeProjectPrivateGithubOnly\": false,\n      \"reDiscoverPrebuilt\": false,\n      \"forExistingProjects\": {\n        \"mrType\": \"patch\",\n        \"commitMessage\": \"init commit\",\n        \"targetBranch\": \"master\"\n      },\n      \"versionControlService\": \"gitlab\",\n      \"projectName\": \"My Example Project\",\n      \"groupPath\": \"itentialopensource/pre-built-automations\",\n      \"prebuilt\": \"@itentialopensource/example-prebuilt\"\n    }\n  }\n}",
      "exampleOutput": "{}"
    }
  ],
  "queryOutput": {},
  "name": "Prebuilt Promotion",
  "overview": "Prebuilt Promotion takes a Pre-Built installed on IAP in Admin Essentials and creates a new repository or updates an existing repository by way of a merge or pull request in the specified git version control service i.e., GitLab, Github or Bitbucket Cloud. This repository contains the Pre-Built's IAP components as well as script and pipeline files to verify the Pre-Built's artifact.json file is generated correctly, increments the Pre-Built version, updates CHANGELOG of changes made to the Pre-Built, and optionally promotes the Pre-Built to another IAP environment.",
  "webName": "Prebuilt Promotion",
  "assetType": "Workflow",
  "additionalOverviewDetails": [
    {
      "title": "CI/CD Pipeline Stages",
      "detail": "On running Prebuilt Promotion against a target repository that does not yet exist in the git version control service, Prebuilt Promotion initializes the repository with CI/CD script and pipeline files. Below describes the stages of the CI/CD process set-up:\n\n- `security`: This step ensures that there are no security vulnerabilities in the npm package dependencies of the repository.\n\n- `unit_test`: Validates path links when comparing artifact.json to manifest.json as well as validates manifest.json against its JSON schema definition. This stage is implemented by jobs `schema_validation` for verifying manifest.json and `schemaLinks_validation` for verifying artifact.json against manifest.json in GitHub and GitLab. In Bitbucket Cloud, a single job implements both checks.\n\n- `generate_artifact_for_merge_request`: Creates artifact.json from bundles directory and manifest.json file in repository and commits that file to the candidate branch. This stage is only run on commit to the candidate branch of a merge or pull request.\n\n- `generate_artifact_and_version_bump`: Increases the version of the Pre-Built based on the candidate branch name prefix being set to `patch`, `minor`, or `major`, following [semver](https://semver.org/). If no prefix is found in the branch name, the default semver value is `patch`. Additionally, this stage updates the repository `CHANGELOG.md` with commit changes or merge request information as well as generates a new artifact.json and commits these files to the `main` branch in GitHub or `master` branch in Bitbucket Cloud and GitLab.\n\n- `promote_artifact`: This stage attempts to import the updated artifact.json to a target IAP instance using the CI/CD variables configured. Note that this stage is only run on commit to the `main` branch in GitHub or `master` branch in Bitbucket Cloud and GitLab. Changes to the pipeline file is necessary if need to run this stage on commit to a candidate branch of a merge or pull request."
    }
  ],
  "dependencyConfigurations": [
    {
      "dependency": "GitLab",
      "configuration": "Prebuilt Promotion over GitLab uses the `master` branch as default branch in project created. If another default branch name like `main` is used, it will be necessary to make updates to the `.gitlab-ci.yml` file used to initialize the repository. The content of the `.gitlab-ci.yml` file used when initializing the repository in GitLab is set in the workflow task with summary `Set pipeline file content` early in the workflow `Prebuilt Promotion Gitlab Create Link`. The other pipeline scripts for GitLab are set in the workflow `Prebuilt Promotion Gitlab Add Files`.\n\nFor GitLab, the `PROMOTE` variable must be assigned the value `True` for the promote stage to run in the pipeline. Otherwise, stage is not started.\n\nBecause of auto-generation and commit of artifact.json file to branch during merge request, it may be necessary in GitLab to check the option `Skipped pipelines are considered successful` under `Settings -> Merge requests -> Merge Checks` in GitLab or else users will not be able to approve the GitLab merge request. For more detail, see GitLab documentation [here](https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html#allow-merge-after-skipped-pipelines).\n\nSee GitLab documentation for setting CI/CD variables in the UI [here](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).\n\nTo see pipeline and script files used for GitLab, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/gitlab?ref_type=heads)."
    },
    {
      "dependency": "GitHub",
      "configuration": "Prebuilt Promotion over GitHub uses the `main` branch as default branch in project created. If another default branch name is used, it will be necessary to make updates to the `.github/workflows/main_action.yml` file content. This can be updated in workflow task with summary `Set main_action.yml file content` in the workflow `Prebuilt Promotion Github Add Files`. The workflow `Prebuilt Promotion Github Add Files` is also where changes to other pipeline script files can be made if need be for Prebuilt Promotion over GitHub.\n\nTo configure variables used in actions for CI/CD in GitHub, refer to the GitHub documentation page [here](https://docs.github.com/en/actions/learn-github-actions/variables#about-variables).\n\nBe aware the source branch of a pull request may not be set up to be deleted by default after merge in GitHub. See GitHub documentation [here](https://docs.github.com/en/repositories/configuring-branches-and-merges-in-your-repository/configuring-pull-request-merges/managing-the-automatic-deletion-of-branches) for more detail for how to configure this setting.\n\nRegarding repository names in GitHub, whitespace characters are not allowed.\n\nIn order for the the pipeline job's built in `GITHUB_TOKEN` to be able to commit files such as the generated Pre-Built artifact.json file, it may be necessary to set `Workflow Permissions` to enable the `Read and write permissions` at the organization or individual repository level. Navigate to `Settings -> Actions -> General -> Workflow permissions` for this configuration, or refer to the GitHub documentation [here](https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/enabling-features-for-your-repository/managing-github-actions-settings-for-a-repository#configuring-the-default-github_token-permissions) for more details.\n\nTo see pipeline and script files used for GitHub, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/github?ref_type=heads)."
    },
    {
      "dependency": "Bitbucket Cloud",
      "configuration": "Prebuilt Promotion over Bitbucket Cloud uses the `master` branch as default branch in project created. If another default branch name is used, it will be necessary to make updates to the `bitbucket-pipelines.yml` file content. This file content can be updated in workflow task with summary `Set pipeline yml file content` in the workflow `Prebuilt Promotion Bitbucket Add Files`. The workflow `Prebuilt Promotion Bitbucket Add Files` is also where changes to other pipeline script files can be made if need be for Prebuilt Promotion over Bitbucket Cloud.\n\nBe aware that Bitbucket Cloud repository names must be lowercase, alphanumerical, and may also contain underscores, dashes, or dots.\n\nTo see pipeline and script files used for Bitbucket Cloud, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/bitbucket_cloud?ref_type=heads)."
    },
    {
      "dependency": "Bitbucket Cloud and GitLab CI/CD Variables",
      "configuration": "For the pipeline created by Prebuilt Promotion to work as expected, the CI/CD variables listed below will need to be set in Bitbucket Cloud or GitLab:\n\n- Git Credentials (Required)\n  - `CI_GIT_EMAIL`: Email for user account running the pipeline. This will be used to set up version control access within your pipeline environment. Example value: `example@gitlab.com`\n\n  - `CI_GIT_USERNAME`: Username of user account running the pipeline. This will be used to set up version control access within your pipeline environment. Example value: `ci-bot-user`\n\n  - `ID_RSA`: The value for ID_RSA variable is the private key from an SSH key pair created by a Windows, Linux, or macOS machine. The corresponding public key generated should be added to the account you want to use for committing the generated artifact.json from the pipeline context to the intended git branch. Refer to vendor documentation [here](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account) for adding public SSH key to account in GitLab and [here](https://support.atlassian.com/bitbucket-cloud/docs/set-up-personal-ssh-keys-on-windows/#Provide-Bitbucket-Cloud-with-your-public-key) for adding public SSH key to account in Bitbucket Cloud.\n\n- Host Override (Optional to set)\n  - `CI_ORIGIN_HOSTNAME`: This will be used to set up git access within your pipeline environment. When this variable is not set, the hostname will default to `git@gitlab.com`. Example value: `git@self-hosted-gitlab.com`\n\n  - `CI_ORIGIN_SSH_PORT`: Port that GitLab is running SSH on. This variable should be set if the SSH client connection is not accessible via the default port `22`. Example value: `2224`"
    },
    {
      "dependency": "promote_artifact CI/CD Variables",
      "configuration": "The CI/CD variables listed below need to be defined in order to run the optional `promote_artifact` stage of the pipeline on commits to the `master` branch in Bitbucket Cloud and GitLab or `main` branch in GitHub. The `promote_artifact` stage is used when wanting to import the auto-generated artifact.json file of the Pre-Built to a higher environment target IAP instance after testing of proposed changes in pull or merge request are reviewed and approved.\n\n- `PROMOTE`: This variable determines if the `promote_artifact` stage of the pipeline will run. Example values: `True` or `False`. Only when this value is assigned `True` will the `promote_artifact` stage be run. If any over value is assigned, or the variable is not defined, the `promote_artifact` stage will not run or attempt to import artifact.json to target IAP.\n\n- `IAP_HOSTNAME`: This variable determines the target IAP to attempt to import the auto-generated artifact.json to in the `promote_artifact` stage. Example value: `https://ACME.itential.com`\n\n- `IAP_TOKEN`: This variable is required if using token authentication and not basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. In GitHub this is expected to be configured as a secret variable. Example value: `123_sample_token`\n\n- `IAP_USERNAME`: This variable is required if using basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. Example value: `iap_user@domain.com`. In GitHub this is expected to be configured as a secret variable.\n\n- `IAP_PW`: This variable is required if using basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. In GitHub this is expected to be configured as a secret variable. Example value: `my_password_123`\n\n- `IAP_SSL_CERT`: This variable is required if using two-way SSL verification for HTTPS for target IAP attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. Note if using Cloud IAP, this value can be created by copying from the web browser the IAP server certificate, intermediate certificate, and root certificate to a single file, in that order. In GitHub this is expected to be configured as a secret variable. Example value:\n\n```txt\n-----BEGIN CERTIFICATE-----\n<server certificate>\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\n<intermediate certificate>\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\n<root certificate>\n-----END CERTIFICATE-----\n```\n\n- `IAP_PUSH_TO_LOCAL`: This variable determines the repository configuration to use when importing the auto-generated Pre-Built to the target IAP. Note the [repository configuration setting](https://docs.itential.com/docs/configuring-multiple-pre-built-repositories) is only useable with GitLab. If this variable is not defined or set to `True`, the scope value will default to local. If this variable is set to `False`, the scope is set to the repository specified in the artifact.json file. The repository configuration used when `IAP_PUSH_TO_LOCAL` is set to `True` or not defined is:\n\n```txt\n\"repository\": {\n  \"type\": \"local\",\n  \"hostname\": \"localhost\",\n  \"path\": \"/\"\n}\n```"
    }
  ]
}