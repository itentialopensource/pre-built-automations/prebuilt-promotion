## Table of Contents 

  - [Getting Started](#getting-started)
    - [Helpful Background Information](#helpful-background-information)
    - [Prerequisites](#prerequisites)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Workflow Project](#using-this-workflow-project)
    - [Prebuilt Promotion](#prebuilt-promotion)
  - [Additional Information](#additional-information)
    - [Support](#support)

## Getting Started

This section is helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information

Workflows often include logic that varies from business to business. As a result, we often find that our Workflow Projects are more useful as modular components that can be incorporated into a larger process. In addition, they often can add value as a learning tool on how we integrate with other systems and how we do things within the Itential Automation Platform.

While these can be utilized, you may find more value in using them as a starting point to build around.


### Prerequisites

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **Prebuilt Promotion** has been tested with:

- IAP **2023.2**

### External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bitbucket</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>GitHub</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>GitLab</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>App-Artifacts</td>
      <td></td>
      <td>^6.5.5-2023.2.2</td>
    </tr>
  </tbody>
</table>

### Adapters

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-gitlab">adapter-gitlab</a></td>
      <td>^0.13.5</td>
      <td></td>
    </tr>    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-bitbucket">adapter-bitbucket</a></td>
      <td>^0.5.7</td>
      <td></td>
    </tr>    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-github">adapter-github</a></td>
      <td>^0.6.5</td>
      <td></td>
    </tr>
  </tbody>
</table>

### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Example Project.
- Import the Example Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).

###  Testing

Cypress is generally used to test all Itential Example Projects. While Cypress is an opensource tool, at Itential we have internal libraries that have been built around Cypress to allow us to test with a deployed IAP.

When certifying our Example Projects for a release of IAP we run these tests against the particular version of IAP and create a release branch in GitLab. If you do not see the Example Project available in your version of IAP please contact Itential.

While Itential tests this Example Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Example Project into a development/testing environment in which you can test the Example Project.

## Using this Workflow Project
Workflow Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs. 

### <ins>Prebuilt Promotion</ins>
Prebuilt Promotion takes a Pre-Built installed on IAP in Admin Essentials and creates a new repository or updates an existing repository by way of a merge or pull request in the specified git version control service i.e., GitLab, Github or Bitbucket Cloud. This repository contains the Pre-Built's IAP components as well as script and pipeline files to verify the Pre-Built's artifact.json file is generated correctly, increments the Pre-Built version, updates CHANGELOG of changes made to the Pre-Built, and optionally promotes the Pre-Built to another IAP environment.

Capabilities include:
- Create a new repository or update an existing repository by way of a merge request or pull request in the specified git version control platform i.e., GitLab, Github or Bitbucket Cloud for a Pre-Built as found on the local IAP where Prebuilt Promotion is being run.
- Include the necessary configuration files and scripts to run a CI/CD pipeline if running Prebuilt Promotion against a repository that does not exist on the specified git version control service.



#### Configuring Dependencies
  
##### GitLab

Prebuilt Promotion over GitLab uses the `master` branch as default branch in project created. If another default branch name like `main` is used, it will be necessary to make updates to the `.gitlab-ci.yml` file used to initialize the repository. The content of the `.gitlab-ci.yml` file used when initializing the repository in GitLab is set in the workflow task with summary `Set pipeline file content` early in the workflow `Prebuilt Promotion Gitlab Create Link`. The other pipeline scripts for GitLab are set in the workflow `Prebuilt Promotion Gitlab Add Files`.

For GitLab, the `PROMOTE` variable must be assigned the value `True` for the promote stage to run in the pipeline. Otherwise, stage is not started.

Because of auto-generation and commit of artifact.json file to branch during merge request, it may be necessary in GitLab to check the option `Skipped pipelines are considered successful` under `Settings -> Merge requests -> Merge Checks` in GitLab or else users will not be able to approve the GitLab merge request. For more detail, see GitLab documentation [here](https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html#allow-merge-after-skipped-pipelines).

See GitLab documentation for setting CI/CD variables in the UI [here](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).

To see pipeline and script files used for GitLab, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/gitlab?ref_type=heads).
  
##### GitHub

Prebuilt Promotion over GitHub uses the `main` branch as default branch in project created. If another default branch name is used, it will be necessary to make updates to the `.github/workflows/main_action.yml` file content. This can be updated in workflow task with summary `Set main_action.yml file content` in the workflow `Prebuilt Promotion Github Add Files`. The workflow `Prebuilt Promotion Github Add Files` is also where changes to other pipeline script files can be made if need be for Prebuilt Promotion over GitHub.

To configure variables used in actions for CI/CD in GitHub, refer to the GitHub documentation page [here](https://docs.github.com/en/actions/learn-github-actions/variables#about-variables).

Be aware the source branch of a pull request may not be set up to be deleted by default after merge in GitHub. See GitHub documentation [here](https://docs.github.com/en/repositories/configuring-branches-and-merges-in-your-repository/configuring-pull-request-merges/managing-the-automatic-deletion-of-branches) for more detail for how to configure this setting.

Regarding repository names in GitHub, whitespace characters are not allowed.

In order for the the pipeline job's built in `GITHUB_TOKEN` to be able to commit files such as the generated Pre-Built artifact.json file, it may be necessary to set `Workflow Permissions` to enable the `Read and write permissions` at the organization or individual repository level. Navigate to `Settings -> Actions -> General -> Workflow permissions` for this configuration, or refer to the GitHub documentation [here](https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/enabling-features-for-your-repository/managing-github-actions-settings-for-a-repository#configuring-the-default-github_token-permissions) for more details.

To see pipeline and script files used for GitHub, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/github?ref_type=heads).
  
##### Bitbucket Cloud

Prebuilt Promotion over Bitbucket Cloud uses the `master` branch as default branch in project created. If another default branch name is used, it will be necessary to make updates to the `bitbucket-pipelines.yml` file content. This file content can be updated in workflow task with summary `Set pipeline yml file content` in the workflow `Prebuilt Promotion Bitbucket Add Files`. The workflow `Prebuilt Promotion Bitbucket Add Files` is also where changes to other pipeline script files can be made if need be for Prebuilt Promotion over Bitbucket Cloud.

Be aware that Bitbucket Cloud repository names must be lowercase, alphanumerical, and may also contain underscores, dashes, or dots.

To see pipeline and script files used for Bitbucket Cloud, refer to files as found in the Itential Open Source common-utils directory [here](https://gitlab.com/itentialopensource/pre-built-automations/common-utils/-/tree/master/bitbucket_cloud?ref_type=heads).
  
##### Bitbucket Cloud and GitLab CI/CD Variables

For the pipeline created by Prebuilt Promotion to work as expected, the CI/CD variables listed below will need to be set in Bitbucket Cloud or GitLab:

- Git Credentials (Required)
  - `CI_GIT_EMAIL`: Email for user account running the pipeline. This will be used to set up version control access within your pipeline environment. Example value: `example@gitlab.com`

  - `CI_GIT_USERNAME`: Username of user account running the pipeline. This will be used to set up version control access within your pipeline environment. Example value: `ci-bot-user`

  - `ID_RSA`: The value for ID_RSA variable is the private key from an SSH key pair created by a Windows, Linux, or macOS machine. The corresponding public key generated should be added to the account you want to use for committing the generated artifact.json from the pipeline context to the intended git branch. Refer to vendor documentation [here](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account) for adding public SSH key to account in GitLab and [here](https://support.atlassian.com/bitbucket-cloud/docs/set-up-personal-ssh-keys-on-windows/#Provide-Bitbucket-Cloud-with-your-public-key) for adding public SSH key to account in Bitbucket Cloud.

- Host Override (Optional to set)
  - `CI_ORIGIN_HOSTNAME`: This will be used to set up git access within your pipeline environment. When this variable is not set, the hostname will default to `git@gitlab.com`. Example value: `git@self-hosted-gitlab.com`

  - `CI_ORIGIN_SSH_PORT`: Port that GitLab is running SSH on. This variable should be set if the SSH client connection is not accessible via the default port `22`. Example value: `2224`
  
##### promote_artifact CI/CD Variables

The CI/CD variables listed below need to be defined in order to run the optional `promote_artifact` stage of the pipeline on commits to the `master` branch in Bitbucket Cloud and GitLab or `main` branch in GitHub. The `promote_artifact` stage is used when wanting to import the auto-generated artifact.json file of the Pre-Built to a higher environment target IAP instance after testing of proposed changes in pull or merge request are reviewed and approved.

- `PROMOTE`: This variable determines if the `promote_artifact` stage of the pipeline will run. Example values: `True` or `False`. Only when this value is assigned `True` will the `promote_artifact` stage be run. If any over value is assigned, or the variable is not defined, the `promote_artifact` stage will not run or attempt to import artifact.json to target IAP.

- `IAP_HOSTNAME`: This variable determines the target IAP to attempt to import the auto-generated artifact.json to in the `promote_artifact` stage. Example value: `https://ACME.itential.com`

- `IAP_TOKEN`: This variable is required if using token authentication and not basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. In GitHub this is expected to be configured as a secret variable. Example value: `123_sample_token`

- `IAP_USERNAME`: This variable is required if using basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. Example value: `iap_user@domain.com`. In GitHub this is expected to be configured as a secret variable.

- `IAP_PW`: This variable is required if using basic authentication to attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. In GitHub this is expected to be configured as a secret variable. Example value: `my_password_123`

- `IAP_SSL_CERT`: This variable is required if using two-way SSL verification for HTTPS for target IAP attempt to use the IAP Pre-Built import API to import the artifact.json to the target IAP. Note if using Cloud IAP, this value can be created by copying from the web browser the IAP server certificate, intermediate certificate, and root certificate to a single file, in that order. In GitHub this is expected to be configured as a secret variable. Example value:

```txt
-----BEGIN CERTIFICATE-----
<server certificate>
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
<intermediate certificate>
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
<root certificate>
-----END CERTIFICATE-----
```

- `IAP_PUSH_TO_LOCAL`: This variable determines the repository configuration to use when importing the auto-generated Pre-Built to the target IAP. Note the [repository configuration setting](https://docs.itential.com/docs/configuring-multiple-pre-built-repositories) is only useable with GitLab. If this variable is not defined or set to `True`, the scope value will default to local. If this variable is set to `False`, the scope is set to the repository specified in the artifact.json file. The repository configuration used when `IAP_PUSH_TO_LOCAL` is set to `True` or not defined is:

```txt
"repository": {
  "type": "local",
  "hostname": "localhost",
  "path": "/"
}
```
  



#### CI/CD Pipeline Stages

On running Prebuilt Promotion against a target repository that does not yet exist in the git version control service, Prebuilt Promotion initializes the repository with CI/CD script and pipeline files. Below describes the stages of the CI/CD process set-up:

- `security`: This step ensures that there are no security vulnerabilities in the npm package dependencies of the repository.

- `unit_test`: Validates path links when comparing artifact.json to manifest.json as well as validates manifest.json against its JSON schema definition. This stage is implemented by jobs `schema_validation` for verifying manifest.json and `schemaLinks_validation` for verifying artifact.json against manifest.json in GitHub and GitLab. In Bitbucket Cloud, a single job implements both checks.

- `generate_artifact_for_merge_request`: Creates artifact.json from bundles directory and manifest.json file in repository and commits that file to the candidate branch. This stage is only run on commit to the candidate branch of a merge or pull request.

- `generate_artifact_and_version_bump`: Increases the version of the Pre-Built based on the candidate branch name prefix being set to `patch`, `minor`, or `major`, following [semver](https://semver.org/). If no prefix is found in the branch name, the default semver value is `patch`. Additionally, this stage updates the repository `CHANGELOG.md` with commit changes or merge request information as well as generates a new artifact.json and commits these files to the `main` branch in GitHub or `master` branch in Bitbucket Cloud and GitLab.

- `promote_artifact`: This stage attempts to import the updated artifact.json to a target IAP instance using the CI/CD variables configured. Note that this stage is only run on commit to the `main` branch in GitHub or `master` branch in Bitbucket Cloud and GitLab. Changes to the pipeline file is necessary if need to run this stage on commit to a candidate branch of a merge or pull request.



#### Entry Point IAP Component

The primary IAP component to run **Prebuilt Promotion** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Prebuilt Promotion</td>
      <td>Operations Manager Automation</td>
    </tr>
  </tbody>
</table>

#### Inputs

The following table lists the inputs for **Prebuilt Promotion**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>versionControlService</td>
      <td>string</td>
      <td>yes</td>
      <td>Version control adapter used to push the Pre-Built files to the respective version control service, i.e., Bitbucket Cloud, GitHub, or GitLab.</td>
      <td><pre lang="json">gitlab</pre></td>
    </tr>    <tr>
      <td>projectName</td>
      <td>string</td>
      <td>yes</td>
      <td>The project name of the repository as would be found in git version control service. Note that GitHub replaces the whitespace characters with "-" and unexpected behavior may occur if the GitHub project name has any whitespace characters. For Bitbucket Cloud, this name must be lowercase, alphanumerical, and may also contain underscores, dashes, or dots.</td>
      <td><pre lang="json">example-project</pre></td>
    </tr>    <tr>
      <td>groupPath</td>
      <td>string</td>
      <td>yes</td>
      <td>Link to the group or workspace to create the project under. Use Organization Name if this is a GitHub project. This is the name of the Workspace if using Bitbucket Cloud.</td>
      <td><pre lang="json">itentialopensource/pre-built-automations</pre></td>
    </tr>    <tr>
      <td>prebuilt</td>
      <td>string</td>
      <td>yes</td>
      <td>The Pre-Built from the IAP instance's Admin Essentials to push to the git version control service</td>
      <td><pre lang="json">@itentialopensource/example-prebuilt</pre></td>
    </tr>    <tr>
      <td>reDiscoverPrebuilt</td>
      <td>boolean</td>
      <td>no</td>
      <td>Whether or not to perform re-discovery of IAP components by reference from existing IAP components in the Pre-Built selected. This is needed if any IAP components have been added to or deleted from the Pre-Built since committing changes to version control service.</td>
      <td><pre lang="json">false</pre></td>
    </tr>    <tr>
      <td>makeProjectPrivateGithubOnly</td>
      <td>boolean</td>
      <td>no</td>
      <td>Whether or not the repository in GitHub is to be made private. This selection only applies if the repository does not already exist, and it is being created.</td>
      <td><pre lang="json">false</pre></td>
    </tr>    <tr>
      <td>forExistingProjects</td>
      <td>object</td>
      <td>yes</td>
      <td>Contain fields used for updating an existing project which includes "mrType", which is for the type of merge request change being made: patch, minor, or major. "commitMessage", which is commit message to use in version control commit. "targetBranch", the branch to commit the Pre-Built changes to.</td>
      <td><pre lang="json">{
  "mrType": "patch",
  "commitMessage": "init commit",
  "targetBranch": "master"
}</pre></td>
    </tr>
  </tbody>
</table>



#### Outputs

There are no outputs for **Prebuilt Promotion**.


#### Query Output

There are no query output examples for **Prebuilt Promotion**.




#### Example Inputs and Outputs

  
##### Example 1

    
Input:
<pre>{
  "formData": {
    "checkInPrebuilt": {
      "makeProjectPrivateGithubOnly": false,
      "reDiscoverPrebuilt": false,
      "forExistingProjects": {
        "mrType": "patch",
        "commitMessage": "init commit",
        "targetBranch": "master"
      },
      "versionControlService": "gitlab",
      "projectName": "My Example Project",
      "groupPath": "itentialopensource/pre-built-automations",
      "prebuilt": "@itentialopensource/example-prebuilt"
    }
  }
} </pre>

    
    
Output:
<pre>{} </pre>

    
  


#### API Links


<table>
  <thead>
    <tr>
      <th>API Name</th>
      <th>API Documentation Link</th>
      <th>API Link Visibility</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>GitHub API</td>
      <td><a href="https://docs.github.com/en/rest?apiVersion=2022-11-28">https://docs.github.com/en/rest?apiVersion=2022-11-28</a></td>
      <td>Public</td>
    </tr>    <tr>
      <td>GitLab API</td>
      <td><a href="https://docs.gitlab.com/ee/api/rest/">https://docs.gitlab.com/ee/api/rest/</a></td>
      <td>Public</td>
    </tr>    <tr>
      <td>Atlassian - Bitbucket Cloud API</td>
      <td><a href="https://developer.atlassian.com/cloud/bitbucket/rest/intro">https://developer.atlassian.com/cloud/bitbucket/rest/intro</a></td>
      <td>Public</td>
    </tr>
  </tbody>
</table>



---
## Additional Information

### Support
Please use your Itential Customer Success account if you need support when using this Workflow Project.



