# Prebuilt Promotion

## Overview

This Pre-Built Automation bundle contains several example use cases that are applicable when Itential Automation Platform is integrated with a git version control platform, i.e. Bitbucket Cloud, GitHub, or GitLab, using REST API. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suit your needs.


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href='https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-promotion/-/blob/master/documentation/Prebuilt Promotion.md' target='_blank'>Prebuilt Promotion</a></td>
      <td>Prebuilt Promotion takes a Pre-Built installed on IAP in Admin Essentials and creates a new repository or updates an existing repository by way of a merge or pull request in the specified git version control service i.e., GitLab, Github or Bitbucket Cloud. This repository contains the Pre-Built's IAP components as well as script and pipeline files to verify the Pre-Built's artifact.json file is generated correctly, increments the Pre-Built version, updates CHANGELOG of changes made to the Pre-Built, and optionally promotes the Pre-Built to another IAP environment.</td>
    </tr>
  </tbody>
</table>


## External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bitbucket</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>GitHub</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>GitLab</td>
      <td></td>
      <td></td>
    </tr>    <tr>
      <td>App-Artifacts</td>
      <td></td>
      <td>^6.5.5-2023.2.2</td>
    </tr>
  </tbody>
</table>

## Adapters

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-gitlab">adapter-gitlab</a></td>
      <td>^0.13.5</td>
      <td></td>
    </tr>    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-bitbucket">adapter-bitbucket</a></td>
      <td>^0.5.7</td>
      <td></td>
    </tr>    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-github">adapter-github</a></td>
      <td>^0.6.5</td>
      <td></td>
    </tr>
  </tbody>
</table>