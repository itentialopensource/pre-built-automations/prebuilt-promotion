
## 2.0.0 [06-12-2024]

Updates workflows to use 2023.2 workflow canvas
Updates pipeline and script files 
Updates Cypress tests 
Updates tasks to have summary and description 
Updates documentation Adds metadata.json

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!52

2024-06-12 19:49:37 +0000

---

## 1.0.5 [04-23-2024]

Updates master branch to be parity with latest changes to 2023.1 release branch

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!50

2024-04-23 18:57:55 +0000

---

## 1.0.4 [07-20-2023]

* Updated README to add repository config information

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!42

---

## 1.0.3 [07-19-2023]

* Updated pipeline files and cypress test files to use generic docker image as well as updated README for GitLab CI/CD variables and setting up Bitbucket runner.

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!38

---

## 1.0.2 [07-06-2023]

* Update master to 2023.1

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!34

---

## 1.0.1 [06-15-2023]

* Updates task references to GitLab adapter to be job variable instead of static value.

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!32

---

## 1.0.0 [05-24-2023]

* Updated pre-requisites.

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!30

---

## 0.1.1 [03-02-2023]

* Updates CHANGELOG

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!11

---

## 0.1.0 [03-02-2023]

* Enables SSL certificate to be passed in for promotion of pre-built over HTTPS
* Fixes issue where not all projects returned in search across groups in GitLab.
* Updates README with changes

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!10

---

## 0.0.19 [02-17-2023]

* Fixes issue where repository not found in pre-built-automations project by replacing call used to check existence of repository.

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!9

---

## 0.0.18 [01-26-2023]

* patch/2023-01-26T14-24-56

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!8

---

## 0.0.17 [11-07-2022]

* Patch/pblt 418

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!5

---

## 0.0.16 [08-22-2022]

* Added support for Github and Bitbucket

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!3

---

## 0.0.15 [04-18-2022]

* Patch/dsup 1338

See merge request itentialopensource/pre-built-automations/prebuilt-promotion!2

---

## 0.0.14 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.13 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.12 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.11 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.10 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.9 [03-28-2022]

* Update bundles/workflows/Prebuilt Promotion Add Files.json,...

See merge request itentialopensource/pre-built-automations/staging/prebuilt-promotion!1

---

## 0.0.8 [03-25-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.7 [03-25-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [03-25-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [03-25-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.4 [03-25-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [03-24-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [03-23-2022]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n\n\n\n\n
