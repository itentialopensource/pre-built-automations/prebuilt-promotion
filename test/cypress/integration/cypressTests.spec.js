import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';
const PrebuiltPromotionJob0Data = require('../fixtures/stubs/Prebuilt Promotion Job0.json');
const PrebuiltPromotionJob1Data = require('../fixtures/stubs/Prebuilt Promotion Job1.json');
const PrebuiltPromotionJob2Data = require('../fixtures/stubs/Prebuilt Promotion Job2.json');
const PrebuiltPromotionJob3Data = require('../fixtures/stubs/Prebuilt Promotion Job3.json');
const PrebuiltPromotionJob4Data = require('../fixtures/stubs/Prebuilt Promotion Job4.json');
const PrebuiltPromotionJob5Data = require('../fixtures/stubs/Prebuilt Promotion Job5.json');
const PrebuiltPromotionReDiscoverJob6Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job6.json');
const PrebuiltPromotionBitbucketCreateLinkJob7Data = require('../fixtures/stubs/Prebuilt Promotion Bitbucket Create Link Job7.json');
const PrebuiltPromotionReDiscoverJob8Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job8.json');
const PrebuiltPromotionBitbucketCreateLinkJob9Data = require('../fixtures/stubs/Prebuilt Promotion Bitbucket Create Link Job9.json');
const PrebuiltPromotionReDiscoverJob10Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job10.json');
const PrebuiltPromotionGithubCreateLinkJob11Data = require('../fixtures/stubs/Prebuilt Promotion Github Create Link Job11.json');
const PrebuiltPromotionReDiscoverJob12Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job12.json');
const PrebuiltPromotionGithubCreateLinkJob13Data = require('../fixtures/stubs/Prebuilt Promotion Github Create Link Job13.json');
const PrebuiltPromotionReDiscoverJob14Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job14.json');
const PrebuiltPromotionGitlabCreateLinkJob15Data = require('../fixtures/stubs/Prebuilt Promotion Gitlab Create Link Job15.json');
const PrebuiltPromotionReDiscoverJob16Data = require('../fixtures/stubs/Prebuilt Promotion Re-Discover Job16.json');
const PrebuiltPromotionGitlabCreateLinkJob17Data = require('../fixtures/stubs/Prebuilt Promotion Gitlab Create Link Job17.json');
const PrebuiltPromotionBitbucketAddFilesJob18Data = require('../fixtures/stubs/Prebuilt Promotion Bitbucket Add Files Job18.json');
const PrebuiltPromotionGithubCheckOrgRepoJob19Data = require('../fixtures/stubs/Prebuilt Promotion Github Check OrgRepo Job19.json');
const PrebuiltPromotionGithubAddFilesJob20Data = require('../fixtures/stubs/Prebuilt Promotion Github Add Files Job20.json');
const PrebuiltPromotionGithubNewCommitJob21Data = require('../fixtures/stubs/Prebuilt Promotion Github New Commit Job21.json');
const PrebuiltPromotionGithubCreateBranchJob22Data = require('../fixtures/stubs/Prebuilt Promotion Github Create Branch Job22.json');
const PrebuiltPromotionGithubNewCommitJob23Data = require('../fixtures/stubs/Prebuilt Promotion Github New Commit Job23.json');
const PrebuiltPromotionGithubCheckOrgRepoJob24Data = require('../fixtures/stubs/Prebuilt Promotion Github Check OrgRepo Job24.json');
const PrebuiltPromotionGithubAddFilesJob25Data = require('../fixtures/stubs/Prebuilt Promotion Github Add Files Job25.json');
const PrebuiltPromotionGitlabAddFilesJob26Data = require('../fixtures/stubs/Prebuilt Promotion Gitlab Add Files Job26.json');
const PrebuiltPromotionGitlabAddFilesJob27Data = require('../fixtures/stubs/Prebuilt Promotion Gitlab Add Files Job27.json');
const PrebuiltPromotionGithubCommitDataJob28Data = require('../fixtures/stubs/Prebuilt Promotion Github Commit Data Job28.json');
const PrebuiltPromotionGithubCommitDataJob29Data = require('../fixtures/stubs/Prebuilt Promotion Github Commit Data Job29.json');
const PrebuiltPromotionGithubCommitDataJob30Data = require('../fixtures/stubs/Prebuilt Promotion Github Commit Data Job30.json');
const PrebuiltPromotionGithubCommitDataJob31Data = require('../fixtures/stubs/Prebuilt Promotion Github Commit Data Job31.json');

function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });
    // Check if Stub flag is enabled
    if(isStub){
      stubTasks.forEach(stubTask=>{
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }
    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();

  return workflowRunner;
}

// Function to delete the stubbed workflow and reimport it without the stub tasks
function replaceStubTasks(workflowRunner, workflowName) {
    workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: workflowName });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
}

describe('Default: Cypress Tests', function () {
  let prebuiltRunner;
  let PrebuiltPromotionJob0Workflow;
  let PrebuiltPromotionJob1Workflow;
  let PrebuiltPromotionJob2Workflow;
  let PrebuiltPromotionJob3Workflow;
  let PrebuiltPromotionJob4Workflow;
  let PrebuiltPromotionJob5Workflow;
  let PrebuiltPromotionReDiscoverJob6Workflow;
  let PrebuiltPromotionBitbucketCreateLinkJob7Workflow;
  let PrebuiltPromotionReDiscoverJob8Workflow;
  let PrebuiltPromotionBitbucketCreateLinkJob9Workflow;
  let PrebuiltPromotionReDiscoverJob10Workflow;
  let PrebuiltPromotionGithubCreateLinkJob11Workflow;
  let PrebuiltPromotionReDiscoverJob12Workflow;
  let PrebuiltPromotionGithubCreateLinkJob13Workflow;
  let PrebuiltPromotionReDiscoverJob14Workflow;
  let PrebuiltPromotionGitlabCreateLinkJob15Workflow;
  let PrebuiltPromotionReDiscoverJob16Workflow;
  let PrebuiltPromotionGitlabCreateLinkJob17Workflow;
  let PrebuiltPromotionBitbucketAddFilesJob18Workflow;
  let PrebuiltPromotionGithubCheckOrgRepoJob19Workflow;
  let PrebuiltPromotionGithubAddFilesJob20Workflow;
  let PrebuiltPromotionGithubNewCommitJob21Workflow;
  let PrebuiltPromotionGithubCreateBranchJob22Workflow;
  let PrebuiltPromotionGithubNewCommitJob23Workflow;
  let PrebuiltPromotionGithubCheckOrgRepoJob24Workflow;
  let PrebuiltPromotionGithubAddFilesJob25Workflow;
  let PrebuiltPromotionGitlabAddFilesJob26Workflow;
  let PrebuiltPromotionGitlabAddFilesJob27Workflow;
  let PrebuiltPromotionGithubCommitDataJob28Workflow;
  let PrebuiltPromotionGithubCommitDataJob29Workflow;
  let PrebuiltPromotionGithubCommitDataJob30Workflow;
  let PrebuiltPromotionGithubCommitDataJob31Workflow;
  
  before(function () {
    //creates a prebuilt runner for importing the project
    cy.fixture(`../../../artifact.json`).then((data) => {
      prebuiltRunner = new PrebuiltRunner(data);
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob0Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob1Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob2Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob3Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob4Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion.json`).then((data) => {
      PrebuiltPromotionJob5Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob6Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Bitbucket Create Link.json`).then((data) => {
      PrebuiltPromotionBitbucketCreateLinkJob7Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob8Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Bitbucket Create Link.json`).then((data) => {
      PrebuiltPromotionBitbucketCreateLinkJob9Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob10Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Create Link.json`).then((data) => {
      PrebuiltPromotionGithubCreateLinkJob11Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob12Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Create Link.json`).then((data) => {
      PrebuiltPromotionGithubCreateLinkJob13Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob14Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Gitlab Create Link.json`).then((data) => {
      PrebuiltPromotionGitlabCreateLinkJob15Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Re-Discover.json`).then((data) => {
      PrebuiltPromotionReDiscoverJob16Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Gitlab Create Link.json`).then((data) => {
      PrebuiltPromotionGitlabCreateLinkJob17Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Bitbucket Add Files.json`).then((data) => {
      PrebuiltPromotionBitbucketAddFilesJob18Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Check OrgRepo.json`).then((data) => {
      PrebuiltPromotionGithubCheckOrgRepoJob19Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Add Files.json`).then((data) => {
      PrebuiltPromotionGithubAddFilesJob20Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github New Commit.json`).then((data) => {
      PrebuiltPromotionGithubNewCommitJob21Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Create Branch.json`).then((data) => {
      PrebuiltPromotionGithubCreateBranchJob22Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github New Commit.json`).then((data) => {
      PrebuiltPromotionGithubNewCommitJob23Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Check OrgRepo.json`).then((data) => {
      PrebuiltPromotionGithubCheckOrgRepoJob24Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Add Files.json`).then((data) => {
      PrebuiltPromotionGithubAddFilesJob25Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Gitlab Add Files.json`).then((data) => {
      PrebuiltPromotionGitlabAddFilesJob26Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Gitlab Add Files.json`).then((data) => {
      PrebuiltPromotionGitlabAddFilesJob27Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Commit Data.json`).then((data) => {
      PrebuiltPromotionGithubCommitDataJob28Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Commit Data.json`).then((data) => {
      PrebuiltPromotionGithubCommitDataJob29Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Commit Data.json`).then((data) => {
      PrebuiltPromotionGithubCommitDataJob30Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Prebuilt Promotion Github Commit Data.json`).then((data) => {
      PrebuiltPromotionGithubCommitDataJob31Workflow = data;
    });
    
  });

  after(function() {
    prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
  });

  describe('Default: Imports Project', function () {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    it('Default: Should import the project into IAP', function () {
        prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
        prebuiltRunner.importPrebuilt.single({});
    });
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create repository for Pre-Built in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob0Workflow, importWorkflow, isStub, PrebuiltPromotionJob0Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob0Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob0Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob0Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob0Workflow);
      });
    })
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create merge request for Pre-Built changes in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob1Workflow, importWorkflow, isStub, PrebuiltPromotionJob1Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob1Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob1Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob1Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob1Workflow);
      });
    })
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob2Workflow, importWorkflow, isStub, PrebuiltPromotionJob2Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob2Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob2Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob2Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob2Workflow);
      });
    })
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob3Workflow, importWorkflow, isStub, PrebuiltPromotionJob3Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob3Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob3Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob3Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob3Workflow);
      });
    })
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create repository for Pre-Built in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob4Workflow, importWorkflow, isStub, PrebuiltPromotionJob4Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob4Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob4Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob4Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob4Workflow);
      });
    })
  })

  describe('Prebuilt Promotion', function() {
    it.skip('It should create merge request for Pre-Built changes in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionJob5Workflow, importWorkflow, isStub, PrebuiltPromotionJob5Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionJob5Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionJob5Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionJob5Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionJob5Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create repository for Pre-Built in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob6Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob6Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob6Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob6Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob6Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob6Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Bitbucket Create Link', function() {
    it.skip('Prebuilt Promotion Bitbucket Create Link: It should create repository for Pre-Built in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionBitbucketCreateLinkJob7Workflow, importWorkflow, isStub, PrebuiltPromotionBitbucketCreateLinkJob7Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionBitbucketCreateLinkJob7Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionBitbucketCreateLinkJob7Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionBitbucketCreateLinkJob7Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionBitbucketCreateLinkJob7Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create merge request for Pre-Built changes in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob8Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob8Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob8Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob8Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob8Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob8Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Bitbucket Create Link', function() {
    it.skip('Prebuilt Promotion Bitbucket Create Link: It should create merge request for Pre-Built changes in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionBitbucketCreateLinkJob9Workflow, importWorkflow, isStub, PrebuiltPromotionBitbucketCreateLinkJob9Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionBitbucketCreateLinkJob9Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionBitbucketCreateLinkJob9Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionBitbucketCreateLinkJob9Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionBitbucketCreateLinkJob9Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob10Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob10Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob10Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob10Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob10Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob10Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Create Link', function() {
    it.skip('Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCreateLinkJob11Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCreateLinkJob11Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCreateLinkJob11Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCreateLinkJob11Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCreateLinkJob11Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCreateLinkJob11Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob12Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob12Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob12Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob12Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob12Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob12Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Create Link', function() {
    it.skip('Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCreateLinkJob13Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCreateLinkJob13Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCreateLinkJob13Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCreateLinkJob13Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCreateLinkJob13Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCreateLinkJob13Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create repository for Pre-Built in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob14Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob14Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob14Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob14Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob14Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob14Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Gitlab Create Link', function() {
    it.skip('Prebuilt Promotion Gitlab Create Link: It should create repository for Pre-Built in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGitlabCreateLinkJob15Workflow, importWorkflow, isStub, PrebuiltPromotionGitlabCreateLinkJob15Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGitlabCreateLinkJob15Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGitlabCreateLinkJob15Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGitlabCreateLinkJob15Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGitlabCreateLinkJob15Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Re-Discover', function() {
    it.skip('Prebuilt Promotion Re-Discover: It should create merge request for Pre-Built changes in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionReDiscoverJob16Workflow, importWorkflow, isStub, PrebuiltPromotionReDiscoverJob16Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionReDiscoverJob16Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionReDiscoverJob16Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionReDiscoverJob16Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionReDiscoverJob16Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Gitlab Create Link', function() {
    it.skip('Prebuilt Promotion Gitlab Create Link: It should create merge request for Pre-Built changes in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGitlabCreateLinkJob17Workflow, importWorkflow, isStub, PrebuiltPromotionGitlabCreateLinkJob17Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGitlabCreateLinkJob17Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGitlabCreateLinkJob17Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGitlabCreateLinkJob17Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGitlabCreateLinkJob17Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Bitbucket Add Files', function() {
    it('Prebuilt Promotion Bitbucket Add Files: Prebuilt Promotion Bitbucket Create Link: It should create repository for Pre-Built in Bitbucket Cloud', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionBitbucketAddFilesJob18Workflow, importWorkflow, isStub, PrebuiltPromotionBitbucketAddFilesJob18Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionBitbucketAddFilesJob18Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionBitbucketAddFilesJob18Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionBitbucketAddFilesJob18Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionBitbucketAddFilesJob18Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Check OrgRepo', function() {
    it.skip('Prebuilt Promotion Github Check OrgRepo: Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCheckOrgRepoJob19Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCheckOrgRepoJob19Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCheckOrgRepoJob19Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCheckOrgRepoJob19Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCheckOrgRepoJob19Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCheckOrgRepoJob19Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Add Files', function() {
    it.skip('Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubAddFilesJob20Workflow, importWorkflow, isStub, PrebuiltPromotionGithubAddFilesJob20Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubAddFilesJob20Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubAddFilesJob20Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubAddFilesJob20Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubAddFilesJob20Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github New Commit', function() {
    it.skip('Prebuilt Promotion Github New Commit: Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubNewCommitJob21Workflow, importWorkflow, isStub, PrebuiltPromotionGithubNewCommitJob21Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubNewCommitJob21Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubNewCommitJob21Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubNewCommitJob21Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubNewCommitJob21Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Create Branch', function() {
    it.skip('Prebuilt Promotion Github Create Branch: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCreateBranchJob22Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCreateBranchJob22Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCreateBranchJob22Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCreateBranchJob22Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCreateBranchJob22Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCreateBranchJob22Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github New Commit', function() {
    it.skip('Prebuilt Promotion Github New Commit: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubNewCommitJob23Workflow, importWorkflow, isStub, PrebuiltPromotionGithubNewCommitJob23Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubNewCommitJob23Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubNewCommitJob23Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubNewCommitJob23Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubNewCommitJob23Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Check OrgRepo', function() {
    it.skip('Prebuilt Promotion Github Check OrgRepo: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCheckOrgRepoJob24Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCheckOrgRepoJob24Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCheckOrgRepoJob24Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCheckOrgRepoJob24Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCheckOrgRepoJob24Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCheckOrgRepoJob24Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Add Files', function() {
    it.skip('Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubAddFilesJob25Workflow, importWorkflow, isStub, PrebuiltPromotionGithubAddFilesJob25Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubAddFilesJob25Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubAddFilesJob25Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubAddFilesJob25Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubAddFilesJob25Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Gitlab Add Files', function() {
    it('Prebuilt Promotion Gitlab Add Files: Prebuilt Promotion Gitlab Create Link: It should create repository for Pre-Built in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGitlabAddFilesJob26Workflow, importWorkflow, isStub, PrebuiltPromotionGitlabAddFilesJob26Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGitlabAddFilesJob26Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGitlabAddFilesJob26Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGitlabAddFilesJob26Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGitlabAddFilesJob26Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Gitlab Add Files', function() {
    it('Prebuilt Promotion Gitlab Add Files: Prebuilt Promotion Gitlab Create Link: It should create merge request for Pre-Built changes in GitLab', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGitlabAddFilesJob27Workflow, importWorkflow, isStub, PrebuiltPromotionGitlabAddFilesJob27Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGitlabAddFilesJob27Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGitlabAddFilesJob27Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGitlabAddFilesJob27Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGitlabAddFilesJob27Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Commit Data', function() {
    it.skip('Prebuilt Promotion Github Commit Data: Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCommitDataJob28Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCommitDataJob28Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCommitDataJob28Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCommitDataJob28Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCommitDataJob28Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCommitDataJob28Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Commit Data', function() {
    it.skip('Prebuilt Promotion Github Commit Data: Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create repository for Pre-Built in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCommitDataJob29Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCommitDataJob29Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCommitDataJob29Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCommitDataJob29Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCommitDataJob29Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCommitDataJob29Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Commit Data', function() {
    it.skip('Prebuilt Promotion Github Commit Data: Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCommitDataJob30Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCommitDataJob30Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCommitDataJob30Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCommitDataJob30Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCommitDataJob30Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCommitDataJob30Workflow);
      });
    })
  })

  describe('Prebuilt Promotion Github Commit Data', function() {
    it.skip('Prebuilt Promotion Github Commit Data: Prebuilt Promotion Github Add Files: Prebuilt Promotion Github Create Link: It should create pull request for Pre-Built changes in GitHub', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PrebuiltPromotionGithubCommitDataJob31Workflow, importWorkflow, isStub, PrebuiltPromotionGithubCommitDataJob31Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PrebuiltPromotionGithubCommitDataJob31Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PrebuiltPromotionGithubCommitDataJob31Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PrebuiltPromotionGithubCommitDataJob31Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PrebuiltPromotionGithubCommitDataJob31Workflow);
      });
    })
  })
});
